Can't Stop AI
=============

Inroduction
-----------

This is a school project, the goal was to develop an "AI" to win a tournament.
The targeted game was [Can't Stop](https://en.wikipedia.org/wiki/Can%27t_Stop_%28board_game%29).
Initial implementation was done by students, but this one was provided by our teachers.
The "AI" is implemented in the file [strategies/Stra194.java](src/main/java/strategies/Strat194.java)

Development
-----------

- Download the project: `git clone`
- Run and observe: `./gradlew run`

Contributors
------------

homersimpsons
