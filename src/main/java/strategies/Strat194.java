package strategies;

import cantstop.Jeu;
import java.util.Random;

/**
 * L'ensemble des stratégies utilisées est facilement modifiable grâce aux configurations en en-têtes.
 * La stratégie de choix se base sur différents niveaux limitant les choix jusqu'à ce qu'il n'en reste plus qu'un.
 * Le choix retenu est :
 *  1. On sélectionne les choix permettant de gagner une ou plusieurs voies
 *  2. On avance toujours le plus possible
 *  3. On utilise une méthode Heuristic pour déterminer:
 *      - où poser les bonzes (le plus de probabilités de pouvoir continuer)
 *      - où avancer (en fonction de l'avancement sur chaque voie du choix)
 *  4. On choisit les voies les plus avancées
 *  5. Puis celle où l'adversaire à le moins de chance de nous bloquer (indépendamment de la vitesse)
 *  5. Puis celle où l'adversaire à le moins de chance de nous bloquer (avec la vitesse)
 * De même il y a plusieurs fonction stop, mais une seule est retenue :
 *  On s'arrête en fonction de la probabilité d'arriver jusqu'au tour actuel avec ces 3 bonzes
 *  et nous prenons plus de risque dans le cas où l'adversaire n'as pas encore marqué de point.
 *  Nous avons aussi des conditions d'arrêts obligées:
 *      - Lorsque nous avons gagné la partie
 *      - Lorsqu'une voie est bloquée
 * Nous avons laissé les recherches d'heuristiques profondes (sur les lancers suivants)
 * car nous n'avons pas réussi à en extraire des résultats probans (que l'on utilise une moyenne théorique de progression, ou un maximum)
 * De plus cette méthode était beaucoup trop longue (à partir de 5 degrés) pour pouvoir être appliquée, nous avons donc essayer de l'activer
 * uniquement lorsqu'une voie peut être bloquée, mais ceci ne permettait pas d'anticiper assez.
 *
 *
 * RENDU: Ce fichier, correctement nommé et rempli avec le numéro de la stratégie choisie.
 *
 * @author homersimpsons
 */
public final class Strat194 implements Strategie {
    // La classe helper avec des fonctions/probas utiles
    private final Helper HELPER;

    // ----------- CHOIX DES STRATÉGIES ----------- \\
    // Sratégie de choix d'arrêt
    // 0 : On s'arrête à chaque tour
    // 1 : On s'arrête aléatoirement
    // 2 : Arrêt par rapport à la probabilité de continuer
    // 3 : Arrêt par rapport au progrès total du tour en cours
    // 4 : Par rapport au progrès de chaque colonne
    // 5 : Par rapport au progrès de chaque colonne et leur longueur
    // 6 : Rule of 28
    // 7 : Basic stop
    // 8 : Probabilités de continuer + adversaire
    private final int STOP_STRAT = 8;

    // Stratégie pour appliquer le choix
    // 0 : Toujours le choix 0
    // 1 : Choix aléatoire
    // 2 : Progression finale
    // 3 : Progression finale avec risque d'être bloqué
    // 4 : Progression finale avec risque d'être bloqué et probabilitée de victoire
    // 5 : Basic Progress
    // 6 : Heuristic
    // 7 : Depth Heuristic
    // 8 : Limit new bonzes
    // 9 : Toujours avancer le plus possible (si tous les bonzes sont posés)
    //10 : Choisir les choix bloquant des colonnes
    private final int[] CHOICE_STRAT = {10, 9, 6, 2, 4, 3}; // liste des strats de choix, tant que la comparaison ne donne pas de strat gagnante on relance avec les autres strats , 3, 1

    // ---------- PARAMÈTRES STRATÉGIES ---------- \\
    // Proba max pour s'arrêter
    private final double PROBA_MAX_STOP = 0.78;
    // Number of game for Heuristic
    // private final int NB_HEURISTIC_GAME = 1000;
    // Depth number
    private final int HEURISTIC_DEPTH = 3;
    // MAX or SUM choice deciding (MAX = true)
    private final boolean MAX = true;
    // Number for stop Rule of 28
    private final int RULE28 = 28;
    // S'arrêter dès qu'une voie est bloquée?
    private final boolean SAFE_PROGRESS = true;
    // Éviter voie avec trop peu de chances
    private final boolean AVOID_MIN_PROBA = true;

    // Paramètres pour AVOID_MIN_PROBA
    private final double probaFaibleGain = 0.15;
    private final double nbLancerAdverse = 6.;

    public Strat194() {
        HELPER = new Helper();
    }

    /**
     * @param choix le choix considéré
     * @return la position des 3 bonzes ayant le plus de probabilité d'être posés
     */
    private int[] bestHeuristicTriplet(int choix) {
        int voieBonze1 = HELPER.emplacementBonze(choix, 1);
        int voieBonze2 = 0;
        int voieBonze3 = 0;
        int nbMarkerChoice = HELPER.markerOnBoard(choix);
        double maxProbaRealise = 0.;
        if (nbMarkerChoice >= 2) {
            voieBonze2 = HELPER.emplacementBonze(choix, 2);
            if (nbMarkerChoice == 3) {
                voieBonze3 = HELPER.emplacementBonze(choix, 3);
                return HELPER.sortColumn(voieBonze1, voieBonze2, voieBonze3);
            }
            for (int i = 0; i < 11; i ++) {
                if ((i != voieBonze1) && (i != voieBonze2) && !HELPER.isBloqueOuFini(i)) {
                    if (HELPER.getProbaSimple(i) > maxProbaRealise) {
                        voieBonze3 = i;
                        maxProbaRealise = HELPER.getProbaSimple(i);
                    }
                }
            }
            return HELPER.sortColumn(voieBonze1, voieBonze2, voieBonze3);
        }

        for (int i = 0; i < 11; i++) {
            if ((i != voieBonze1) && !HELPER.isBloqueOuFini(i)) {
                for (int j = i + 1; j < 11; j++) {
                    if ((j != voieBonze1) && !HELPER.isBloqueOuFini(j)) {
                        if (HELPER.getProbaEtDouble(i, j) > maxProbaRealise) {
                            voieBonze2 = i;
                            voieBonze3 = j;
                            maxProbaRealise = HELPER.getProbaEtDouble(i, j);
                        }
                    }
                }
            }
        }
        return HELPER.sortColumn(voieBonze1, voieBonze2, voieBonze3);
    }

    /**
     * @param choix le choix considéré
     * @return la probabilité maximum du choix
     */
    private double bestHeuristicProba(int choix) {
        int[] bonzes = bestHeuristicTriplet(choix);
        return maxProbaVoie(choix, bonzes);
    }

    /**
     * @param choix le choix considéré
     * @param bonzes l'emplacement des bonzes considérés
     * @return la probabilitée maximum de gagner avec ces bonzes
     */
    private double maxProbaVoie(int choix, int[] bonzes) {
        double probaTriple = HELPER.getProbaTriple(bonzes[0], bonzes[1], bonzes[2]);
        double probaGainMax = 0.;
        double probaGainVoie;
        for(int i = 0; i < 3; i++) {
            probaGainVoie = Math.pow(probaTriple, HELPER.nbLancerPourGagnerVoie(choix, bonzes[i]));
            if (probaGainVoie > probaFaibleGain || HELPER.nbLancersNecessaireGainVoieParAdversaire(bonzes[i]) > nbLancerAdverse) {
                if (probaGainVoie > probaGainMax) {
                    probaGainMax = probaGainVoie;
                }
            }
        }
        return probaGainMax;
    }

    /**
     * @param paramJeu le jeu en cours
     * @return le choix effectué
     */
    public int choix(Jeu paramJeu) {
        HELPER.setParamJeu(paramJeu, true);
        if (HELPER.getNbChoix() == 1) { return 0; } // On optimise lorsque l'on a pas le choix
        boolean notUniqueChoice = true;
        double[][] choicesProba = new double[6][2];
        boolean[] choixPossibles = new boolean[HELPER.getNbChoix()];
        for (int i = 0; i < choixPossibles.length; i++) {
            choixPossibles[i] = true;
        }
        boolean isAnyBlocked = blockedChoice();
        int choice_strat;
        int index_choice = 0;
        while (notUniqueChoice && index_choice < CHOICE_STRAT.length) {
            choice_strat = CHOICE_STRAT[index_choice];
            index_choice++;
            for (int tmp_choix = 0; tmp_choix < HELPER.getNbChoix(); tmp_choix++) {
                if (choixPossibles[tmp_choix]) {
                    switch (choice_strat) {
                        case 0:
                            return 0;
                        case 1:
                            Random rand = new Random();
                            choicesProba[tmp_choix][0] = rand.nextDouble();
                            choicesProba[tmp_choix][1] = 0.;
                            break;
                        case 2:
                            choicesProba[tmp_choix][0] = HELPER.nbLancerAvancement(tmp_choix, HELPER.getNormChoice(tmp_choix, 0));
                            if (HELPER.isNotNullChoice(tmp_choix, 1) && HELPER.getNormChoice(tmp_choix, 0) != HELPER.getNormChoice(tmp_choix, 1)) {
                                choicesProba[tmp_choix][1] = HELPER.nbLancerAvancement(tmp_choix, HELPER.getNormChoice(tmp_choix, 1));
                            }
                            break;
                        case 3:
                            choicesProba[tmp_choix][0] = HELPER.nbLancerAvancement(tmp_choix, HELPER.getNormChoice(tmp_choix, 0))
                                    * Math.pow(HELPER.getVitesseVoie(HELPER.getNormChoice(tmp_choix, 0)), HELPER.nbLancerAvancementAdversaire(HELPER.getNormChoice(tmp_choix, 0)) - HELPER.nbLancerAvancement(tmp_choix, HELPER.getNormChoice(tmp_choix, 0)));
                            if (HELPER.isNotNullChoice(tmp_choix, 1)) {
                                choicesProba[tmp_choix][1] = HELPER.nbLancerAvancement(tmp_choix, HELPER.getNormChoice(tmp_choix, 1))
                                        * Math.pow(HELPER.getVitesseVoie(HELPER.getNormChoice(tmp_choix, 1)), HELPER.nbLancerAvancementAdversaire(HELPER.getNormChoice(tmp_choix, 1)) - HELPER.nbLancerAvancement(tmp_choix, HELPER.getNormChoice(tmp_choix, 1)));
                            }
                            break;
                        case 4:
                            choicesProba[tmp_choix][0] = HELPER.nbLancerAvancement(tmp_choix, HELPER.getNormChoice(tmp_choix, 0))
                                    * (1. + (HELPER.nbLancerAvancement(tmp_choix, HELPER.getNormChoice(tmp_choix, 0))/23) - (HELPER.nbLancerAvancementAdversaire(HELPER.getNormChoice(tmp_choix, 0))/23));
                            if (HELPER.isNotNullChoice(tmp_choix, 1)) {
                                choicesProba[tmp_choix][1] = HELPER.nbLancerAvancement(tmp_choix, HELPER.getNormChoice(tmp_choix, 1))
                                        * (1. + (HELPER.nbLancerAvancement(tmp_choix, HELPER.getNormChoice(tmp_choix, 1))/23) - (HELPER.nbLancerAvancementAdversaire(HELPER.getNormChoice(tmp_choix, 1))/23));
                            }
                            break;
                        case 5:
                            choicesProba[tmp_choix][0] = 6.5 - ((double)Math.abs(5 - HELPER.getNormChoice(tmp_choix, 0)));
                            if (HELPER.isNotNullChoice(tmp_choix, 1)) {
                                choicesProba[tmp_choix][1] = 6.5 - ((double)Math.abs(5 - HELPER.getNormChoice(tmp_choix, 1)));
                            }
                            break;
                        case 6:
                            if (HELPER.getBonzesRestants() != 0) {
                                choicesProba[tmp_choix][0] = bestHeuristicProba(tmp_choix); // /((double)(HELPER.markerOnBoard(tmp_choix)-HELPER.getBonzesRestants()+1))
                                choicesProba[tmp_choix][1] = 0.;
                            } else {
                                choicesProba[tmp_choix][0] = Math.pow(HELPER.getProbaTripleUnordered(HELPER.getNormBonze(0), HELPER.getNormBonze(1), HELPER.getNormBonze(2)), HELPER.nbLancerPourGagnerVoie(tmp_choix, HELPER.getNormChoice(tmp_choix, 0)));
                                if (HELPER.isNotNullChoice(tmp_choix, 1)) {
                                    choicesProba[tmp_choix][1] = Math.pow(HELPER.getProbaTriple(HELPER.emplacementBonze(tmp_choix, 1), HELPER.emplacementBonze(tmp_choix, 2), HELPER.emplacementBonze(tmp_choix, 3)), HELPER.nbLancerPourGagnerVoie(tmp_choix, HELPER.getNormChoice(tmp_choix, 1)));
                                } else {
                                    choicesProba[tmp_choix][1] = 0.;
                                }
                            }
                            break;
                        case 7:
                            if (HELPER.getBonzesRestants() == 0 && isAnyBlocked) {
                                choicesProba[tmp_choix][0] = choiceHeuristic(tmp_choix);
                            } else {
                                choicesProba[tmp_choix][0] = 0.;
                            }
                            choicesProba[tmp_choix][1] = 0.;
                            break;
                        case 8:
                            if (HELPER.getBonzesRestants() == 0) {
                                choicesProba[tmp_choix][0] = .5;
                            } else {
                                choicesProba[tmp_choix][0] = .5 - ((double)HELPER.markerOnBoard(tmp_choix));
                            }
                            choicesProba[tmp_choix][1] = 0.;
                            break;
                        case 9:
                            if (HELPER.getBonzesRestants() == 0) {
                                choicesProba[tmp_choix][0] = .5;
                                if (HELPER.isNotNullChoice(tmp_choix, 1) || HELPER.getBonzesRestants() > 0) {
                                    choicesProba[tmp_choix][1] = .9; // > .5
                                } else {
                                    choicesProba[tmp_choix][1] = 0.;
                                }
                            } else {
                                choicesProba[tmp_choix][0] = .5;
                                choicesProba[tmp_choix][1] = .5;
                            }
                            break;
                        case 10:
                            if (HELPER.nbLancerPourGagnerVoie(tmp_choix, HELPER.getNormChoice(tmp_choix, 0)) == 0.) {
                                choicesProba[tmp_choix][0] = 1.;
                            } else {
                                choicesProba[tmp_choix][0] = 0.;
                            }
                            if (HELPER.getNormChoice(tmp_choix, 0) != HELPER.getNormChoice(tmp_choix, 1) && HELPER.isNotNullChoice(tmp_choix, 1) && HELPER.nbLancerPourGagnerVoie(tmp_choix, HELPER.getNormChoice(tmp_choix, 1)) == 0.) {
                                choicesProba[tmp_choix][1] = 1.;
                            } else {
                                choicesProba[tmp_choix][1] = 0.;
                            }
                    }
                    if (!HELPER.isNotNullChoice(tmp_choix, 1)) {
                        choicesProba[tmp_choix][1] = 0.;
                    }
                } else {
                    choicesProba[tmp_choix][0] = 0.;
                    choicesProba[tmp_choix][1] = 0.;
                }
            }
            notUniqueChoice = compareChoice(choicesProba, choixPossibles);
        }
        for (int i = 0; i < HELPER.getNbChoix(); i++) {
            if (choixPossibles[i]) { return i; }
        }
        return 0; // n'arrive jamais
    }

    /**
     * @return un choix bloque-t-il une voie ?
     */
    private boolean blockedChoice() {
        for (int i = 0; i < HELPER.getNbChoix(); i++) {
            for (int j = 0; j < 2; j++) {
                if (HELPER.isNotNullChoice(i, j) && HELPER.nbLancerPourGagnerVoie(i, HELPER.getNormChoice(i, j)) == 0.) {
                    return true;
                }
            }
        }
        return false;
    }

    // choicesProba values (pour les probas normalisées):
    // 1. = sur de gagner cette voie
    // 0. = choix impossible
    /**
     * @param choicesProba le tableau des valeurs des choix
     * @param choices les choix possibles
     * @return le choix est-il unique ?
     */
    private boolean compareChoice(double[][] choicesProba, boolean[] choices) {
        double max_proba = -1.;
        for(int i = 0; i < HELPER.getNbChoix(); i++) {
            if(choices[i]) {
                if (AVOID_MIN_PROBA && (choicesProba[i][0] < probaFaibleGain) && (HELPER.nbLancersNecessaireGainVoieParAdversaire(HELPER.getNormChoice(i, 0)) < nbLancerAdverse)) {
                    choicesProba[i][0] /= 2; // 0.
                }
                if (HELPER.isNotNullChoice(i, 1)) {
                    if (AVOID_MIN_PROBA && (choicesProba[i][1] < probaFaibleGain) && (HELPER.nbLancersNecessaireGainVoieParAdversaire(HELPER.getNormChoice(i, 1)) < nbLancerAdverse)) {
                        choicesProba[i][1] /= 2; // 0.
                    }
                }
                if ((choicesProba[i][0] == 1.) && (choicesProba[i][1] == 1.)) { // Si on est sûr de gagner les 2 voies on choisit ce choix
                    choicesProba[i][0] = 200.;
                    choicesProba[i][1] = 0.;
                }
                if ((choicesProba[i][0] == 1.) || (choicesProba[i][1] == 1.)) { // Si on est sûr de gagner au moins une voie on choisit temporairement ce choix
                    choicesProba[i][0] = 100.;
                    choicesProba[i][1] = 0.;
                    max_proba = 100.;
                }

                if (MAX) { // En maximum si activé
                    if ((choicesProba[i][0] > max_proba) || (choicesProba[i][1] > max_proba)) {
                        max_proba = Math.max(choicesProba[i][0], choicesProba[i][1]);
                    }
                } else { // Ou en somme si activé
                    if (choicesProba[i][0] + choicesProba[i][1] > max_proba) {
                        max_proba = choicesProba[i][0] + choicesProba[i][1];
                    }
                }
            }
        }
        for (int i = 0; i < choices.length; i++) {
            if (choices[i]) {
                if(MAX) {
                    choices[i] = (choicesProba[i][0] == max_proba) || (choicesProba[i][1] == max_proba);
                } else {
                    choices[i] = (choicesProba[i][0] + choicesProba[i][1] == max_proba);
                }
            }
        }
        return moreThanOneTrue(choices);
    }

    /**
     * @param tab le tableau à tester
     * @return il y a-t-il plus d'un vrai dans le tableau ?
     */
    private boolean moreThanOneTrue(boolean[] tab) {
        int i = 0;
        for (int j = 0; j < tab.length; j++) {
            if (tab[j]) {
                i++;
                if (i > 1) { return true; }
            }
        }
        return false;
    }

    /**
     * @param paramJeu le jeu en cours
     * @return arrêter ?
     */
    public boolean stop(Jeu paramJeu){
        HELPER.setParamJeu(paramJeu, false);
        return stop(HELPER);
    }

    /**
     * @param HELPER le Helper à utiliser pour le stop
     * @return arrêter ?
     */
    private boolean stop(Helper HELPER) {
        if (HELPER.score(true) + HELPER.getNbBloque() >= 3) { return true; } // On sarrête obligatoirement si on gagne
        if (HELPER.getBonzesRestants() != 0) { return false; } // On n'arrête pas tant qu'il reste des bonzes
        if (HELPER.getNbBloque() > 0 && SAFE_PROGRESS) { return true; } // On s'arrête dès qu'une voie est bloquée si activé
        int[] voies = new int[3 - HELPER.getNbBloque()];
        for(int i = 0; i < voies.length; i++) {
            voies[i] = HELPER.emplacementUnblockedBonze(i);
        }
        switch(STOP_STRAT) {
            case 0 : return true;
            case 1 :
                Random rand = new Random();
                return rand.nextBoolean();
            case 2 : return HELPER.getProbaVoies(voies, false) < PROBA_MAX_STOP;
            case 3 : return HELPER.getProbaVoies(voies, false) * (HELPER.getAvancementTour() + 1) + HELPER.getProbaVoies(voies, true) * (HELPER.getAvancementTour() + 2) > HELPER.getAvancementTour();
            case 4 : return shouldColumnProgressStop(voies, false, HELPER);
            case 5 : return shouldColumnProgressStop(voies, true, HELPER);
            case 6 : return rule28(HELPER) > RULE28;
            case 7 : return HELPER.getNbBloque() > 0;
            case 8 : return shouldStopAdv(HELPER);
        }
        return true;
    }

    /**
     * @param HELPER Le Helper à utiliser
     * @return arrêter ?
     */
    private boolean shouldStopAdv(Helper HELPER) {
        double probaLancers = 0.;
        switch(HELPER.getNbBloque()) {
            case 0 :
                probaLancers = HELPER.probaTripleApresNLancers(HELPER.getNormBonze(0), HELPER.getNormBonze(1), HELPER.getNormBonze(2), HELPER.getNbCoup());
                break;
            case 1 :
                probaLancers = HELPER.probaDoubleApresNLancers(HELPER.emplacementUnblockedBonze(0), HELPER.emplacementUnblockedBonze(1), HELPER.getNbCoup()) / 9;
                break;
            case 2:
                probaLancers = HELPER.probaSimpleApresNLancers(HELPER.emplacementUnblockedBonze(0), HELPER.getNbCoup()) / 36;
                break;
        }
        double probaAutreJoueur = HELPER.getScoreAutreJoueur() == 0 ? 0.2 : 0.05; // 0.2 - HELPER.getScoreAutreJoueur()*0.07
        return (probaLancers < probaAutreJoueur);
    }

     /**
     * @return la valeur pour la Règle du 28
     */
    private int rule28(Helper HELPER) {
        int res = 0;
        int parite = 0;
        int inf8 = 0;
        int sup6 = 0;
        for (int i = 0; i < 3; i++) { // on a normalement les 3 bonzes placés
            int voie = HELPER.getNormBonze(i);
            parite = parite + ( voie%2==0 ? -1 : 1);
            if (voie < 8) { inf8++; }
            if (voie > 6) { sup6++; }
            res = res + ( 15 - HELPER.getMaximum(voie) ) + (((15 - HELPER.getMaximum(voie))/2) * HELPER.getVoieAvancement(voie,true));
        }
        if (Math.abs(parite) == 3) {
            res = res + ((parite/3) * 2);
        }
        if (inf8 == 3 || sup6 ==3) {
            res += 4;
        }
        return res;
    }

    /**
     * @param voies les 3 voies concernés
     * @param vitesses prendre en compte ou pas la vitesse des colonnes
     * @return arrêter ?
     */
    private boolean shouldColumnProgressStop(int[] voies, boolean vitesses, Helper HELPER) {
        double c = 0;
        for(int i = 0; i < voies.length; i++){
            for(int j = i; j < voies.length; j++) {
                for(int terme = 0; terme < voies.length; terme++) {
                    c += probaVoieTermeValue(voies[i],-1,voies[terme],vitesses); // termes simples
                    c += probaVoieTermeValue(voies[i],voies[j],voies[terme],vitesses);
                }
            }
        }
        return c > HELPER.getAvancementTour();
    }


    /**
     * @param voie_i 1ère voie
     * @param voie_j 2ème voie (ou -1 si une seule voie)
     * @param terme le terme considérer
     * @param vitesses avec les poids ?
     * @return indicateur sur la progression
     */
    private double probaVoieTermeValue(int voie_i, int voie_j, int terme, boolean vitesses) {
        if(voie_j==-1) {
            return HELPER.getProbaSimple(voie_i) * (vitesses?HELPER.getVitesseVoie(terme):1) * ( HELPER.getVoieAvancement(terme,true) + (terme==voie_i?1:0));
        }
        if (voie_i==voie_j) {
            return (HELPER.getVitesseVoie(voie_i) - HELPER.getProbaSimple(voie_i)) * (vitesses?HELPER.getVitesseVoie(terme):1) * ( HELPER.getVoieAvancement(terme,true) + (terme==voie_i?2:0) );
        }
        return HELPER.getProbaEtDoubleUnordered(voie_i,voie_j) * (vitesses?HELPER.getVitesseVoie(terme):1) * ( HELPER.getVoieAvancement(terme,true) + ((terme==voie_i||terme==voie_j)?1:0));
    }



    public String getName() {
        return "homersimpsons";
    }

    public int getGroupe() {
        return 6;
    }

    /**
     * @param choix le choix à considérer
     * @return la valeur du choix
     */
    private double choiceHeuristic(int choix) {
        Helper helper = new Helper(HELPER);
        int tmp_avancement = helper.setStep(HELPER.columnToBonze(HELPER.getNormChoice(choix, 0)));
        double avancement = 1.;
        double blocked = 0.;
        double proba = 1.;
        if ( HELPER.isNotNullChoice(choix, 1) && ( HELPER.getNormChoice(choix, 1) != HELPER.getNormChoice(choix, 0) || tmp_avancement != 2)) {
            avancement++;
            helper.setStep(HELPER.columnToBonze(HELPER.getNormChoice(choix, 1)));
        }
        if (stop(helper)) {
            blocked = helper.getNbBloque();
            proba = 1.;
            return blocked/3;
//            return (((double)avancement)/2 + BLOCKED_VALUE * ((double) helper.getNbBloque() / 3.)) / (1 + BLOCKED_VALUE); // A améliorer
        } else {
            double total_proba = 0.;
            for (int i = 0; i < 3; i++) {
                for(int j = i; j < 3; j++) {
                    total_proba += HELPER.getProbaSimple(HELPER.getNormBonze(i)) +
                                   HELPER.getProbaSimple(HELPER.getNormBonze(j)) +
                                   HELPER.getProbaEtDouble(HELPER.getNormBonze(i), HELPER.getNormBonze(j));
                }
            }
            heuristicInfos[] tmp_heuristics = heuristicStep(helper, 0, false, total_proba);
            blocked = ((double) tmp_heuristics[0].getNbBloque()) / tmp_heuristics[0].getNbPossible();
            avancement = ((double) tmp_heuristics[0].getNbAvancement()) / tmp_heuristics[0].getNbPossible();
            proba = tmp_heuristics[0].getProba();
            return ((double)tmp_heuristics[2].getNbBloque());
        }
    }

    /**
     * @param HELPER Le Helper à utiliser
     * @param depth la profondeur actuelle de récursion
     * @param STOP est-ce la dernière étape (stop à renvoyer faux)
     * @param total_proba les probas totales sur les 3 voies
     * @return Un tableau décrit au début de la fonction
     */ // Les étapes de l'heuristique
    private heuristicInfos[] heuristicStep(Helper HELPER, int depth, boolean STOP, double total_proba) {
        heuristicInfos[] heuristic = { new heuristicInfos(1.), // Le plus de proba
                                       new heuristicInfos(1.), // Le plus d'avancement
                                       new heuristicInfos(1.)};// Le plus de bloque
        if (STOP || depth > HEURISTIC_DEPTH) { return heuristic; }
        if ( depth > HEURISTIC_DEPTH ) {
            heuristic[2].setNbBloque(1); //On suppose qu'on pourra, à terme, en bloquer 1
            return heuristic;
        }
        STOP = stop(HELPER);
        for(int k = 0; k < heuristic.length; k++) {
            heuristic[k] = new heuristicInfos(0.);
        }
        heuristicInfos[] tmp_heuristic = { new heuristicInfos(1.),
                                           new heuristicInfos(1.)};
        Helper[] tmp_helper = new Helper[2];
        int tmp_avancement;
        for (int i = 0; i < 3; i++) {
            for(int j = i; j < 3; j++) {
                if (!HELPER.isBloque(HELPER.getNormBonze(i)) || !HELPER.isBloque(HELPER.getNormBonze(j))) {
                    tmp_helper[0] = new Helper(HELPER);
                    tmp_helper[1] = new Helper(HELPER);
                    if (HELPER.isBloque(HELPER.getNormBonze(i))) {
                        tmp_heuristic[0] = new heuristicInfos(HELPER.getProbaSimple(HELPER.getNormBonze(j)));
                        tmp_avancement = tmp_helper[0].setStep(j);
                        tmp_heuristic[0].setStep(tmp_avancement);
                        if ( tmp_avancement != 2 ) {
                            tmp_helper[1].setStep(j);
                            tmp_heuristic[1] = new heuristicInfos(HELPER.getProbaEtDouble(HELPER.getNormBonze(j), HELPER.getNormBonze(j)));
                            tmp_heuristic[1].setStep(tmp_helper[1].setStep(j));
                        } else {
                            tmp_heuristic[1] = null;
                            tmp_helper[1] = null;
                        }
                    } else if (HELPER.isBloque(HELPER.getNormBonze(j))) {
                        tmp_heuristic[0] = new heuristicInfos(HELPER.getProbaSimple(HELPER.getNormBonze(i)));
                        tmp_avancement = tmp_helper[0].setStep(i);
                        tmp_heuristic[0].setStep(tmp_avancement);
                        if ( tmp_avancement != 2 ) {
                            tmp_helper[1].setStep(i);
                            tmp_heuristic[1] = new heuristicInfos(HELPER.getProbaEtDouble(HELPER.getNormBonze(i), HELPER.getNormBonze(i)));
                            tmp_heuristic[1].setStep(tmp_helper[1].setStep(i));
                        } else {
                            tmp_heuristic[1] = null;
                            tmp_helper[1] = null;
                        }
                    } else {
                        tmp_heuristic[0] = new heuristicInfos(HELPER.getProbaEtDoubleUnordered(HELPER.getNormBonze(i), HELPER.getNormBonze(j)));
                        tmp_heuristic[0].setStep(tmp_helper[0].setStep(i));
                        tmp_heuristic[0].setStep(tmp_helper[0].setStep(j));
                        tmp_heuristic[1] = null;
                        tmp_helper[1] = null;
                    }
                    for (int nbHelper = 0; nbHelper < tmp_helper.length; nbHelper++) {
                        if (tmp_helper[nbHelper] != null) {
                            heuristicInfos[] depthHeuristic = heuristicStep(tmp_helper[nbHelper], depth + 1, STOP, total_proba);
                            for (int k = 0; k < tmp_heuristic.length; k++) {
                                if ( tmp_heuristic[k] != null) {
                                    heuristic[0].update(tmp_heuristic[k], depthHeuristic[0], true);
                                    if (depthHeuristic[1].getNbAvancement() + tmp_heuristic[k].getNbAvancement() > heuristic[1].getNbAvancement()) {
                                        heuristic[1] = new heuristicInfos(tmp_heuristic[k], depthHeuristic[1]);
                                    } else  if (depthHeuristic[1].getNbAvancement() + tmp_heuristic[k].getNbAvancement() == heuristic[1].getNbAvancement()) {
                                        heuristic[1].update(tmp_heuristic[k], depthHeuristic[1], false);
                                    }
                                    if (depthHeuristic[2].getNbBloque() + tmp_heuristic[k].getNbBloque() > heuristic[2].getNbBloque()) {
                                        heuristic[2] = new heuristicInfos(tmp_heuristic[k], depthHeuristic[2]);
                                    } else if (depthHeuristic[2].getNbBloque() + tmp_heuristic[k].getNbBloque() == heuristic[2].getNbBloque()) {
                                        heuristic[2].update(tmp_heuristic[k], depthHeuristic[2], false);
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        for (int i = 0; i < heuristic.length; i++) {
            heuristic[i].normalize(total_proba);
        }
        return heuristic;
    }

    // Classe pour stocker les informations sur les heuristiques
    private class heuristicInfos {
        private int nbAvancement;
        private int nbBloque;
        private double proba;
        private int nbPossible;

        heuristicInfos(double _proba) {
            proba = _proba;
            nbAvancement = 0;
            nbBloque = 0;
            nbPossible = 1;
        }

        heuristicInfos(heuristicInfos tmp_heuristic, heuristicInfos depthHeuristic) {
            proba = tmp_heuristic.getProba() * depthHeuristic.getProba();
            nbAvancement = tmp_heuristic.getNbAvancement() + depthHeuristic.getNbAvancement();
            nbBloque = tmp_heuristic.getNbBloque() + depthHeuristic.getNbBloque();
            nbPossible = depthHeuristic.getNbPossible(); //tmp_heuristic.getNbPossible() +
        }

        public void update(heuristicInfos tmp_heuristic, heuristicInfos depthHeuristic, boolean probUpdate) {
            proba += tmp_heuristic.getProba() * depthHeuristic.getProba();
            if (probUpdate) {
                nbAvancement += tmp_heuristic.getNbAvancement() + depthHeuristic.getNbAvancement();
                nbBloque += tmp_heuristic.getNbBloque() + depthHeuristic.getNbBloque();
                nbPossible += depthHeuristic.getNbPossible(); //tmp_heuristic.getNbPossible() +
            }
        }

        public void setStep(int i) {
            if ( i > 0 ) {
                nbAvancement++;
                if ( i == 2 ) {
                    nbBloque++;
                }
            }
        }

        public void normalize(double total_proba) {
            proba = proba / total_proba;
        }

        public int getNbAvancement() { return nbAvancement; }
        public void setNbBloque(int i) {nbBloque = i; }
        public int getNbBloque() { return nbBloque; }
        public double getProba() { return proba; }
        public int getNbPossible() { return nbPossible; }
        public String toString() {
            return "a=" + nbAvancement + "\tb=" + nbBloque + "\tp=" + proba + "\tnb=" + nbPossible;
        }
    }

    // Classe avec de nombreuses fonctions utiles concernant le jeu
    private class Helper {
        // proba d'avoir une somme 1 fois au moins (Wikipedia)
        private final double[] PROBA_SIMPLE = {171./1296, 302./1296, 461./1296, 580./1296, 727./1296, 837./1296, 727./1296, 580./1296, 461./1296, 302./1296, 171./1296};
        // proba d'avoir une somme (avec doublon) (Wikipedia)
        private final double[] VITESSE_VOIE = {172./1296, 308./1296, 480./1296, 616./1296, 788./1296, 924./1296, 788./1296, 616./1296, 480./1296, 308./1296, 172};
        // proba d'avoir exactement ces 2 sommes dans un choix du tirage (sans doublon) (Tableur)
        private final double[][] PROBA_ET_DOUBLE = {{1./1296, 4./1296, 10./1296, 16./1296, 22./1296, 28./1296 , 30./1296 , 24./1296, 18./1296, 12./1296, 6./1296 },
                                                    {0.     , 6./1296, 16./1296, 24./1296, 36./1296, 48./1296 , 48./1296 , 48./1296, 36./1296, 24./1296, 12./1296},
                                                    {0.     , 0.     , 19./1296, 40./1296, 62./1296, 76./1296 , 70./1296 , 60./1296, 54./1296, 36./1296, 18./1296},
                                                    {0.     , 0.     , 0.      , 36./1296, 76./1296, 96./1296 , 88./1296 , 72./1296, 60./1296, 48./1296, 24./1296},
                                                    {0.     , 0.     , 0.      , 0.      , 61./1296, 124./1296, 110./1296, 88./1296, 70./1296, 48./1296, 30./1296},
                                                    {0.     , 0.     , 0.      , 0.      , 0.      , 90./1296 , 124./1296, 96./1296, 76./1296, 48./1296, 28./1296},
                                                    {0.     , 0.     , 0.      , 0.      , 0.      , 0.       , 61./1296 , 76./1296, 62./1296, 36./1296, 22./1296},
                                                    {0.     , 0.     , 0.      , 0.      , 0.      , 0.       , 0.       , 36./1296, 40./1296, 24./1296, 16./1296},
                                                    {0.     , 0.     , 0.      , 0.      , 0.      , 0.       , 0.       , 0.      , 19./1296, 16./1296, 10./1296},
                                                    {0.     , 0.     , 0.      , 0.      , 0.      , 0.       , 0.       , 0.      , 0.      , 6./1296 , 4./1296 },
                                                    {0.     , 0.     , 0.      , 0.      , 0.      , 0.       , 0.       , 0.      , 0.      , 0.      , 1./1296 }};
        // proba d'avoir l'une de ces 2 sommes dans un choix du tirage (sans doublon) (Tableur)
        private final double[][] PROBA_OU_DOUBLE = {{171./1296, 415./1296, 568./1296, 681./1296, 822./1296, 923./1296 , 868./1296 , 727./1296 , 614./1296, 461./1296, 336./1296},
                                                    {0.       , 302./1296, 609./1296, 690./1296, 825./1296, 920./1296 , 885./1296 , 834./1296 , 727./1296, 580./1296, 461./1296},
                                                    {0.       , 0.       , 461./1296, 791./1296, 934./1296, 997./1296 , 968./1296 , 885./1296 , 868./1296, 727./1296, 614./1296},
                                                    {0.       , 0.       , 0.       , 580./1296, 949./1296, 1006./1296, 997./1296 , 896./1296 , 885./1296, 834./1296, 727./1296},
                                                    {0.       , 0.       , 0.       , 0.       , 727./1296, 1083./1296, 1068./1296, 997./1296 , 968./1296, 885./1296, 868./1296},
                                                    {0.       , 0.       , 0.       , 0.       , 0.       , 834./1296 , 1083./1296, 1006./1296, 997./1296, 920./1296, 923./1296},
                                                    {0.       , 0.       , 0.       , 0.       , 0.       , 0.        , 727./1296 , 949./1296 , 934./1296, 825./1296, 822./1296},
                                                    {0.       , 0.       , 0.       , 0.       , 0.       , 0.        , 0.        , 580./1296 , 791./1296, 690./1296, 681./1296},
                                                    {0.       , 0.       , 0.       , 0.       , 0.       , 0.        , 0.        , 0.        , 461./1296, 609./1296, 568./1296},
                                                    {0.       , 0.       , 0.       , 0.       , 0.       , 0.        , 0.        , 0.        , 0.       , 302./1296, 415./1296},
                                                    {0.       , 0.       , 0.       , 0.       , 0.       , 0.        , 0.        , 0.        , 0.       , 0.       , 171./1296}};
        // proba d'avoir au moins une somme dans les choix (sans doublon)
        private double[][][] PROBA_TRIPLE = new double[11][11][11];
        // le nombre de case de chaque colone
        private final int[] MAXIMUM = {3, 5, 7, 9, 11, 13, 11, 9, 7, 5, 3};
        // le jeu en cours
        private Jeu paramJeu;
        // On stocke localement les choix (optimisation)
        private int[][] lesChoix;
        // On stocke localement les choix (optimisation)
        private int[][] bonzes;
        // On stock localement les bloque/fini (optimisation)
        private boolean[] bloque;
        private boolean[] fini;
        // notre avancement
        private int[] avancement;
        // le nombre de bloque
        private int nbBloque;
        // le nombre de coup joués
        private int nbCoup;
        // les scores
        int myScore, OpoScore;
        // le nombre de bonzes restants
        int bonzesRestants;
        // l'avancement de l'adversaire
        private int[] oponentAvancement;
        // le nombre de fois où l'adversaire n'as pas avancer
        private int nbOponentFail;
        // Une estimation du nombre de fois où l'adversaire lance les dés
        private double averageOponentRoll;
        //
        private int overAllRoll;

        Helper() {
            calculProba(); // On génére les probabilités de pouvoir continuer car trop long/complexe à rentrer manuellement
        }

        Helper(Helper HELPER) {
            PROBA_TRIPLE = HELPER.PROBA_TRIPLE;
            bonzes = HELPER.getNewBonzes();
            avancement = HELPER.getNewAvancement();
            nbBloque = HELPER.nbBloque;
            nbCoup = HELPER.nbCoup + 1;
            bloque = HELPER.getNewBloque();
            myScore = HELPER.score(true);
            OpoScore = HELPER.score(false);
            bonzesRestants = HELPER.getBonzesRestants();
        }

        /**
         * @param _paramJeu le jeu en cours
         * @param choice choix ou stop ? (choix = true)
         * Permet d'initialiser le jeu avec le jeu en cours
         */
        public void setParamJeu(Jeu _paramJeu, boolean choice) {
            if ( _paramJeu.getNbCoup() == 0 ) {
                averageOponentRollNumber(_paramJeu);
                fini = _paramJeu.getFini();
                avancement = _paramJeu.avancementJoueurEnCours();
                myScore = _paramJeu.scoreJoueurEnCours();
                OpoScore = _paramJeu.scoreAutreJoueur();
                bonzesRestants = _paramJeu.getBonzesRestants();
            }
            paramJeu = _paramJeu;
            bloque = paramJeu.getBloque();
            bonzes = paramJeu.getBonzes();
            if (choice) {
                lesChoix = paramJeu.getLesChoix();
                nbCoup = paramJeu.getNbCoup();
            } else {
                bonzesRestants = paramJeu.getBonzesRestants();
                nbBloque = nbBloque();
            }
        }

        /**
         * @param current joueur en cours ou adversaire ? (courrant = true)
         * @return le score du joueur
         */
        public int score(boolean current) {
            return current ? myScore : OpoScore;
        }

        /**
         * @param choix le choix
         * @param item le choix
         * @return le choix normalisé [0;10]
         */
        public int getNormChoice(int choix, int item) {
            return lesChoix[choix][item] - 2;
        }

        /**
         * @param bonze le bonze
         * @return la voie du bonze normalisée [0;10]
         */
        public int getNormBonze(int bonze) {
            return bonzes[bonze][0] - 2;
        }

        /**
         * @param bonze le bonze
         * @return code indiquant l'état
         */ // Simule l'avancement du bonze dans ce Helper
        public int setStep(int bonze) {
            if (!bloque[getNormBonze(bonze)]) {
                bonzes[bonze][1]++;
                if (bonzes[bonze][1] == getMaximum(getNormBonze(bonze))) {
                    bloque[getNormBonze(bonze)] = true;
                    nbBloque++;
                    return 2; // valeurs d'une voie bloquee
                }
                return 1; // valeur d'un avancement
            }
            return 0; // aucun avancement
        }

        public int[][] getNewBonzes() {
            int[][] bonzes = new int[this.bonzes.length][this.bonzes[0].length];
            for(int i = 0; i < this.bonzes.length; i++) {
                bonzes[i] = this.bonzes[i].clone();
            }
            return bonzes;
        }

        public int[] getNewAvancement() {
            return avancement.clone();
        }

        public boolean[] getNewBloque() {
            return bloque.clone();
        }

        // --------------------- PROBA APRES N LANCERS --------------------- \\

        /**
         * @param somme somme considérée
         * @param n Nombre de lancers
         * @return La probabilité davoir le triplet après n lancers
         */
        public double probaSimpleApresNLancers(int somme, int n) {
            return Math.pow(getProbaSimple(somme), n);
        }

        /**
         * @param somme1 1ère somme (ordre croissant)
         * @param somme2 2ème somme (ordre croissant)
         * @param n Nombre de lancers
         * @return La probabilité davoir le triplet après n lancers
         */
        public double probaDoubleApresNLancers(int somme1, int somme2, int n) {
            return Math.pow(getProbaOuDoubleUnordered(somme1, somme2), n);
        }

        /**
         * @param somme1 1ère somme (ordre croissant)
         * @param somme2 2ème somme (ordre croissant)
         * @param somme3 3ème somme (ordre croissant)
         * @param n Nombre de lancers
         * @return La probabilité davoir le triplet après n lancers
         */
        public double probaTripleApresNLancers(int somme1, int somme2, int somme3, int n) {
            return Math.pow(getProbaTripleUnordered(somme1, somme2, somme3), n);
        }

        // ----------------------- PROBABILITES VOIES ---------------------- \\

        /**
         * @param somme la somme
         * @return la probabilité d'avoir cette somme au moins une fois
         */
        public double getProbaSimple(int somme) {
            return PROBA_SIMPLE[somme];
        }

        /**
         * @param somme1 1ère somme (ordre croissant)
         * @param somme2 2ème somme (ordre croissant)
         * @return la probabilité d'avoir exactement ces 2 sommes dans un choix du tirage (sans doublon)
         */
        public double getProbaEtDouble(int somme1, int somme2) {
            return PROBA_ET_DOUBLE[somme1][somme2];
        }

        /**
         * @param somme1 1ère somme (ordre croissant)
         * @param somme2 2ème somme (ordre croissant)
         * @return la probabilité d'avoir exactement ces 2 sommes dans un choix du tirage (sans doublon)
         */
        public double getProbaEtDoubleUnordered(int somme1, int somme2) {
            return PROBA_ET_DOUBLE[Math.min(somme1, somme2)][Math.max(somme1, somme2)];
        }

        /**
         * @param somme1 1ère somme (ordre croissant)
         * @param somme2 2ème somme (ordre croissant)
         * @return la probabilité d'avoir l'une de ces 2 sommes dans un choix du tirage (sans doublon)
         */
        public double getProbaOuDouble(int somme1, int somme2) {
            return PROBA_OU_DOUBLE[somme1][somme2];
        }

        /**
         * @param somme1 1ère somme
         * @param somme2 2ème somme
         * @return la probabilité d'avoir l'une de ces 2 sommes dans un choix du tirage (sans doublon)
         */
        public double getProbaOuDoubleUnordered(int somme1, int somme2) {
            return PROBA_OU_DOUBLE[Math.min(somme1, somme2)][Math.max(somme1, somme2)];
        }

        /**
         * @param somme1 1ère somme (ordre croissant)
         * @param somme2 2ème somme (ordre croissant)
         * @param somme3 3ème somme (ordre croissant)
         * @return la probabilitée d'avoir au moins une somme
         */
        public double getProbaTriple(int somme1, int somme2, int somme3) {
            return PROBA_TRIPLE[somme1][somme2][somme3];
        }

        /**
         * @param somme1 1ère somme
         * @param somme2 2ème somme
         * @param somme3 3ème somme
         * @return la probabilitée d'avoir au moins une somme
         */
        public double getProbaTripleUnordered(int somme1, int somme2, int somme3) {
            int min = Math.min(somme1, Math.min(somme2, somme3));
            int max = Math.max(somme1, Math.max(somme2, somme3));
            int med = (somme1 != min && somme1 != max) ? somme1 : (somme2 != min && somme2 != max) ? somme2 : somme3;
            return PROBA_TRIPLE[min][med][max];
        }

        /**
         * @param voie la voie
         * @return la probabilité d'avoir cette somme (avec doublon) (représente la vitesse)
         */
        public double getVitesseVoie(int voie) {
            return VITESSE_VOIE[voie];
        }

        // ------------------------ CONTEXTE DU JEU ------------------------ \\


        public int getNbChoix() {
            return paramJeu.getNbChoix();
        }

        public int getNbCoup() {
            return nbCoup;
        }

        public int getScoreAutreJoueur() {
            return OpoScore;
        }

        public int getBonzesRestants() {
            return bonzesRestants;
        }

        public int getMaximum(int i) {
            return MAXIMUM[i];
        }

        // ----------------------------- BONZES ---------------------------- \\

        public int getBonze(int bonze, int item) {
            return bonzes[bonze][item];
        }

        /**
         * @param choix choix a considérer
         * @param n n° bonze
         * @return emplacement du n-ième bonze (ordre croissant)
         */
        public int emplacementBonze(int choix, int n) {
            int i = 0;
            for (int j = 0; j < 11; j++) {
                // On compte si un bonze ou un choix va sur cette voie
                if ((isMarkerOnColumn(j)) || (getNormChoice(choix,0) == j) || (getNormChoice(choix,1) == j)) { i++; }
                if (n == i) { return j; }
            }
            return 0; // jamais retourné normalement
        }

        /**
         * @param n le n° de bonze
         * @return la voie du n-ième bonze non bloqué
         */
        public int emplacementUnblockedBonze(int n) {
            int i = 0;
            for (int j = 0; j < bonzes.length; j++) {
                if (!isBloque(getNormBonze(j))) { i++; }
                if (n == i) { return getNormBonze(j); }
            }
            return 0; // jamais retourné normalement
        }

        /**
         * @param choix choix a tester
         * @return Nombre de bonzes placés si on effectue ce choix
         */
        public int markerOnBoard(int choix) {
            int i = 3 - paramJeu.getBonzesRestants();
            if (!isMarkerOnColumn(getNormChoice(choix,0))) { i++; }
            if ((isNotNullChoice(choix,1)) && (lesChoix[choix][0] != lesChoix[choix][1]) && (!isMarkerOnColumn(getNormChoice(choix,1)))) { i++; }
            return i;
        }

        /**
         * @param voie la voie concernée
         * @return il y a-t-il un bonze sur la voie ?
         */
        private boolean isMarkerOnColumn(int voie) {
            for (int i = 0; i < 3 - getBonzesRestants(); i++) {
                if (bonzes[i][0] - 2 == voie) { return true; }
            }
            return false;
        }

        /**
         * @param voie
         * @return le numéro du bonze situé sur cette voie
         */
        public int columnToBonze(int voie) {
            for (int k = 0; k < bonzes.length; k++) {
                if (getNormBonze(k) == voie) {
                    return k;
                }
            }
            return -1; // Jamais retourné
        }

        // ---------------------------- LANCERS ---------------------------- \\

        /**
         * @param choix le choix considéré
         * @param voie la voie considéré
         * @return la valeur de l'avancement sur cette voie
         */
        public double nbLancerAvancement(int choix, int voie) {
            double tmp_avancement = avancementSurLaVoie(voie);
            if (getNormChoice(choix,0) == voie) { tmp_avancement += 1.; }
            if (getNormChoice(choix,1) == voie) { tmp_avancement += 1.; }
            if (tmp_avancement >= MAXIMUM[voie]) { return 1.; }
            return tmp_avancement / VITESSE_VOIE[voie];
        }

        /**
         * @param choix choix a tester
         * @param voie voie sélectionnée
         * @return valeur de l'avancement restant
         */
        public double nbLancerPourGagnerVoie(int choix, int voie) {
            double tmp_avancement = MAXIMUM[voie] - avancementSurLaVoie(voie);
            if (getNormChoice(choix,0) == voie) { tmp_avancement -= 1.; }
            if (getNormChoice(choix,1) == voie) { tmp_avancement -= 1.; }
            if (tmp_avancement <= 0.) { return 0.; }
            return tmp_avancement / VITESSE_VOIE[voie];
        }

        /**
         * @param voie voie a tester
         * @return l'avancement courrant
         */
        private int avancementSurLaVoie(int voie) {
            for (int i = 0; i < 3; i++) {
                if (getNormBonze(i) == voie) { return bonzes[i][1]; }
            }
            return avancement[voie];
        }

        /**
         * @param voie
         * @return la valeur de l'avancement de l'adversaire sur cette voie
         */
        public double nbLancerAvancementAdversaire(int voie) {
            return ((double) oponentAvancement[voie]) / VITESSE_VOIE[voie];
        }

        /**
         * @param voie la voie a tester
         * @return valeur davancement de l'adversaire
         */
        public double nbLancersNecessaireGainVoieParAdversaire(int voie) {
            int casesRestantes = MAXIMUM[voie] - oponentAvancement[voie];
            return ((double) casesRestantes) / VITESSE_VOIE[voie];
        }

        /**
         * @param somme1 1ère somme (ordre croissant)
         * @param somme2 2ème somme (ordre croissant)
         * @param somme3 3ème somme (ordre croissant)
         * @return Le nombre de lancer avant que la probabilitée soit inférieure à 0,5
         */
        public int nbLancerPossible50(int somme1, int somme2, int somme3) {
            return (int) (Math.log(0.5) / Math.log(PROBA_TRIPLE[somme1][somme2][somme3])) + 1;
        }

        // ------------------------- BLOQUE / FINI ------------------------- \\

        /**
         * @return une voie est-elle bloquée ?
         */
        public boolean isCompleteColumn() {
            for (int i = 0; i < 11; i++) {
                if (bloque[i]) { return true; }
            }
            return false;
        }

        /**
         * @return le nombre de voie bloquees ou finies du jeu
         */
        public int nbBloqueFini() {
            int res = 0;
            for (int i = 0; i < bloque.length; i++) {
                if ( isBloqueOuFini(i) ) { res++; }
            }
            return res;
        }

        /**
         * @return le nombre de voies bloquées
         */
        private int nbBloque() {
            int res = 0;
            for (int i = 0; i < bloque.length; i++) {
                if ( isBloque(i) ) { res++; }
            }
            return res;
        }

        public int getNbBloque() {
            return nbBloque;
        }

        /**
         * @param voie la voie a tester
         * @return renvoie si la voie est bloquee/finie ou non
         */
        public boolean isBloqueOuFini(int voie) {
            return (bloque[voie] || fini[voie]);
        }

        /**
         * @param voie la voie a tester
         * @return renvoie si la voie est bloquee ou non
         */
        public boolean isBloque(int voie) {
            return bloque[voie];
        }

        // ------------------------------ VRAC ----------------------------- \\

        /**
         * @param choix
         * @param item
         * @return le choix est-il non null ?
         */
        public boolean isNotNullChoice(int choix, int item) {
            return lesChoix[choix][item] != 0;
        }

        /**
         * @param voie1 1ère voie
         * @param voie2 2ème voie
         * @param voie3 3ème voie
         * @return les voies triées dans l'ordre croissant
         */
        public int[] sortColumn(int voie1, int voie2, int voie3) {
            int[] column = new int[3];
            column[0] = Math.min(voie1, Math.min(voie2, voie3));
            column[2] = Math.max(voie1, Math.max(voie2, voie3));
            if ((voie1 != column[0]) && (voie1 != column[2])) { column[1] = voie1; }
            if ((voie2 != column[0]) && (voie2 != column[2])) { column[1] = voie2; }
            if ((voie3 != column[0]) && (voie3 != column[2])) { column[1] = voie3; }
            return column;
        }

        public int getAvancementTour() {
            int av = 0;
            for(int i = 0; i < bonzes.length; i++) {
                av += bonzes[i][1] - avancement[getNormBonze(i)];
            }
            return av;
        }

        /**
         * @param voie la voie 0 indexed)
         * @param current du tour courrant ou global (tour courrant = true)
         * @return l'avancerVariables
         */
        public int getVoieAvancement(int voie, boolean current) {
            for(int i = 0; i < bonzes.length; i++) {
                if (getNormBonze(i) == voie) {
                    if (current) {
                        return bonzes[i][1] - avancement[getNormBonze(i)];
                    } else {
                        return bonzes[i][1];
                    }
                }
            }
            return current ? 0 : avancement[voie];
        }

        /**
         * @param voies les voies concérnées (<=3) distinctes ! (par ordre croissantes)
         * @param exact progression sur les 1 ou 2 voies en même temps ?
         * @return
         */
        public double getProbaVoies(int[] voies, boolean exact) {
            if (exact) {
                switch(voies.length) {
                    case 1 : return getVitesseVoie(voies[0]) - getProbaSimple(voies[0]);
                    case 2 :
                        return getProbaEtDouble(voies[0], voies[0]) // Évènements disjoints
                                + getProbaEtDouble(voies[1], voies[1])
                                + getProbaEtDouble(voies[0], voies[1]);
                    case 3:
                        return getProbaEtDouble(voies[0], voies[0]) // Évènements disjoints
                                + getProbaEtDouble(voies[1], voies[1])
                                + getProbaEtDouble(voies[2], voies[2])
                                + getProbaEtDouble(voies[0], voies[1])
                                + getProbaEtDouble(voies[0], voies[2])
                                + getProbaEtDouble(voies[1], voies[2]);
                }
            }
            switch(voies.length) {
                case 1 : return getProbaSimple(voies[0]);
                case 2 : return getProbaOuDouble(voies[0], voies[1]);
                case 3 : return getProbaTriple(voies[0], voies[1], voies[2]);
            }
            return -1.; // Non prévu
        }

        // ------------- TRACKNIG DE L'ADVERSAIRE --------------- \\

        private void averageOponentRollNumber(Jeu _paramJeu) {
            if (oponentAvancement == null) {
                oponentAvancement = new int[11];
                for (int i = 0; i < 11; i++) {
                    oponentAvancement[i] = 0;
                }
            }
            int[] newAvancement = _paramJeu.avancementAutreJoueur();
            double avAvancement = 0;
            for (int voie = 0; voie < 11; voie++) {
                avAvancement += computeAverageColumnProgress(oponentAvancement, newAvancement, voie);
            }
            if (avAvancement == 0.) { nbOponentFail++; }
            oponentAvancement = newAvancement;

        }

        private double computeAverageColumnProgress(int[] oldAvancement, int[] newAvancement, int voie) {
            return ((double) newAvancement[voie] - (double) oldAvancement[voie])/VITESSE_VOIE[voie];
        }


        // ------------------ GÉNÉRER PROBAS POUR 3 VOIES ------------------ \\
        private void calculProba() {
            int[][] sommesDes = new int[3][2];
            int somme1;
            int somme2;
            for (int de = 0; de < 6; de++) {
                for (int de2 = 0; de2 < 6; de2++) {
                    for (int de3 = 0; de3 < 6; de3++) {
                        for (int de4 = 0; de4 < 6; de4++) {
                            sommesDes = sommeDePossible(de, de2, de3, de4);
                            for (somme1 = 0; somme1 < 11; somme1++) {
                                for (somme2 = somme1 + 1; somme2 < 11; somme2++) {
                                    for (int somme3 = somme2 + 1; somme3 < 11; somme3++) {
                                        if (isTripleInDiceSums(sommesDes, somme1, somme2, somme3)) {
                                            PROBA_TRIPLE[somme1][somme2][somme3] += 1.;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            // On normalise les résultats par rapport au total de lancers possibles
            for (somme1 = 0; somme1 < 11; somme1++) {
                for (somme2 = somme1 + 1; somme2 < 11; somme2++) {
                    for (int somme3 = somme2 + 1; somme3 < 11; somme3++) {
                        PROBA_TRIPLE[somme1][somme2][somme3] /= 1296;
                    }
                }
            }
        }

        /**
         * @param de1 résultat du 1er dé
         * @param de2 résultat du 2ème dé
         * @param de3 résultat du 3ème dé
         * @param de4 résultat du 4ème dé
         * @return les sommes possibles de dés
         */
        private int[][] sommeDePossible(int de1, int de2, int de3, int de4) {
            int[][] sommesDes = new int[3][2];
            sommesDes[0][0] = (de1 + de2);
            sommesDes[0][1] = (de3 + de4);
            sommesDes[1][0] = (de1 + de3);
            sommesDes[1][1] = (de2 + de4);
            sommesDes[2][0] = (de1 + de4);
            sommesDes[2][1] = (de2 + de3);
            return sommesDes;
        }

        /**
         * @param sommesDes la représentation de la somme des dés
         * @param somme1 1ère somme (ordre croissant)
         * @param somme2 2ème somme (ordre croissant)
         * @param somme3 3ème somme (ordre croissant)
         * @return il y a-t-il un membre du triplet dans la somme ?
         */
        public boolean isTripleInDiceSums(int[][] sommesDes, int somme1, int somme2, int somme3) {
            return (isInDiceSums(sommesDes, somme1)) || (isInDiceSums(sommesDes, somme2)) || (isInDiceSums(sommesDes, somme3));
        }

        /**
         * @param sommesDes la représentation de la somme des dés
         * @param somme la somme à vérifier
         * @return somme dans les sommes de des ?
         */
        public boolean isInDiceSums(int[][] sommesDes, int somme) {
            for (int i = 0; i < 3; i++) {
                for (int j = 0; j < 2; j++) {
                    if (sommesDes[i][j] == somme) { return true; }
                }
            }
            return false;
        }

        // ---------------------- FORMULE DE POINCARÉ ---------------------- \\

        /**
         * @param probas tableau des probas pour la formule de poincaré
         * @return union des probas
         */
        public double poincaré(double[] probas) {
            double prob = 0;
            for (int i = 0; i < probas.length; i++) {
                prob = -prob;
                int[] termes = new int[i+1];
                for (int j = 0; j < termes.length; j++) {
                    termes[j] = j;
                }
                while (!poincaré_lastTerm(termes, probas.length)) {
                    prob += proba_intersection(termes, probas);
                    termes = poincaré_incremTermes(termes, probas.length);
                }
                prob += proba_intersection(termes, probas);
            }
            return Math.abs(prob);
        }

        /**
         * @param termes liste des termes
         * @param nbProbas nb total de probas
         * @return liste des termes incrémentée pour poincaré
         */
        private int[] poincaré_incremTermes(int[] termes, int nbProbas){
            int terme = termes.length - 1;
            while(terme != 0 && termes[terme] == nbProbas - termes.length + terme){
                terme--;
            }
            termes[terme]++;
            for(int i = terme + 1; i < termes.length; i++) {
                termes[i] = termes[i-1] + 1;
            }
            return termes;
        }

        /**
         * @param termes liste des termes choisis
         * @param probas tableau des probas
         * @return probabilité de l'intersection
         */
        private double proba_intersection(int[] termes, double[] probas) {
            double res = 1;
            for(int i = 0; i < termes.length; i++)
            {
                res *= probas[termes[i]];
            }
            return res;
        }

        /**
         * @param termes liste des termes choisis
         * @param nbProbas nb total de probas
         * @return dernieres combinaison ?
         */
        private boolean poincaré_lastTerm(int[] termes, int nbProbas) {
            for (int i = 0; i < termes.length; i++){
                if(termes[i] != nbProbas - termes.length + i) {
                    return false;
                }
            }
            return true;
        }
    }
}
